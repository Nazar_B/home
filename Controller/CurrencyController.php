<?php
class CurrencyController extends Controller
{
    const API = "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5";
    public function getJson()
    {
        $json = file_get_contents(self::API);
        $json = json_decode($json);
        return $json;
    }
    public function indexAction(Request $request)
    {
        foreach ($this->getJson() as $item) {
            if ($item->ccy === "USD") {
                $buyUSD = $item->buy;
                $sellUSD = $item->sale;
            }
            if ($item->ccy === "EUR") {
                $buyEUR = $item->buy;
                $sellEUR = $item->sale;
            }
            if ($item->ccy === "BTC") {
                $buyBTC = $item->buy;
                $sellBTC = $item->sale;
            }
            if ($item->ccy === "RUR") {
                $buyRUR = $item->buy;
                $sellRUR = $item->sale;
            }
        }
        return $this->render('index', [
            'BuyUSD' => $buyUSD,
            'SellUSD' => $sellUSD,
            'BuyEUR' => $buyEUR,
            'SellEUR' => $sellEUR,
            'BuyBTC' => $buyBTC,
            'SellBTC' => $sellBTC,
            'BuyRUR' => $buyRUR,
            'SellRUR' => $sellRUR
        ]);
    }
    public function converterAction()
    {
        $status = isset($_SESSION['status']) ? $_SESSION['status'] : null;

        unset($_SESSION['status']);

        return $this->render('converter', [
            'status' => $status
        ]);
    }
    public function calculateAction(Request $request) {
        if (!$request->isPost()) {
            return false;
        }
        $form1 = $request->post('form1');
        $form2 = $request->post('form2');
        $sum = $request->post('sum');
        foreach ($this->getJson() as $item) {
            if ($item->ccy === "USD") {
                $buyUSD = $item->buy;
                $sellUSD = $item->sale;
            }
            if ($item->ccy === "EUR") {
                $buyEUR = $item->buy;
                $sellEUR = $item->sale;
            }
            if ($item->ccy === "BTC") {
                $buyBTC = $item->buy;
                $sellBTC = $item->sale;
            }
            if ($item->ccy === "RUR") {
                $buyRUR = $item->buy;
                $sellRUR = $item->sale;
            }
        }
        if($form1 === $form2){
            $_SESSION['status'] = 'З '. $sum . ' ' . $form1 . ' ви отримаєте ' . $sum . ' ' . $form2;
        }
        else {
            switch ($form1){
                case 'USD':
                    $sum1 = $buyUSD;
                    break;
                case 'EUR':
                    $sum1 = $buyEUR;
                    break;
                case 'BTC':
                    $sum1 = $buyBTC;
                    break;
                case 'RUR':
                    $sum1 = $buyRUR;
                    break;
            }
            switch ($form2){
                case 'USD':
                    $sum2 = $buyUSD;
                    break;
                case 'EUR':
                    $sum2 = $buyEUR;
                    break;
                case 'BTC':
                    $sum2 = $buyBTC;
                    break;
                case 'RUR':
                    $sum2 = $buyRUR;
                    break;
            }
        }
        $rezaults = ($sum1 / $sum2) * $sum;
        $_SESSION['status'] = 'З '. $sum . ' ' . $form1 . ' ви отримаєте ' . $rezaults . ' ' . $form2;
        return header('Location: ?route=currency/converter');
    }
}